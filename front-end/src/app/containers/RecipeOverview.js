import React from "react";
import PropTypes from 'prop-types';
import RecipeDetailsComponent from "../components/RecipeDetails";

import '../stylesheets/recipe-overview-component.sass'

const RecipeOverview = (props) => {

  const getDataTarget = name => `#${name}`;

  return (
      <div className="panel panel-default">
        <div className="panel-body recipe-name">{props.recipe.name}
          {props.recipe.isVegetarian && <i className="fa fa-leaf vegetarian-icon"></i>}
          <i className="fa fa-plus expand-button" data-toggle="collapse"
             data-target={getDataTarget(props.recipe.id)}>
          </i>
          <RecipeDetailsComponent {...props.recipe}/>
        </div>
      </div>
  )
};

export default RecipeOverview;

RecipeOverview.propTypes = {
  recipe: PropTypes.object.isRequired
};