const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: [
        './app/index.js'
    ],
    output: {
        path: path.resolve(__dirname, "dist/"),
        filename: 'bundle.js',
        publicPath: "/dist"
    },
    devtool: 'sourcemaps',
    cache: true,

    devServer: {
        contentBase: path.join(__dirname, ""),
        compress: true,
        watchContentBase: true,
        historyApiFallback: true,
        port: 3000
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader!autoprefixer-loader!',
            },
            {
                test: /\.sass$/,
                loader: 'style-loader!css-loader!autoprefixer-loader!sass',
            },
            {
                test: /\.less$/,
                loader: 'style-loader!css-loader!autoprefixer-loader!less',
            },
            {
                test: /\.js?$/,
                loaders: ['react-hot/webpack', 'babel'],
                include: path.join(__dirname, 'app')
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                loader: 'url-loader?limit=100000'
            }
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env':{
                'API_URL': JSON.stringify('http://localhost:8082')
            }
        })
    ]
};
