package com.abn.amro.common;

import lombok.Data;

@Data
public class ErrorEntity {

    private final String message;
}
