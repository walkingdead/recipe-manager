package com.abn.amro.common.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

@Profile("test")
@ComponentScan(basePackages = "com.abn.amro")
public class IntegrationTestConfiguration {

}
