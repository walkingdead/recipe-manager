import {get} from 'axios'

const API_URL = process.env.API_URL;

export const getRecipesPageableAndSearch = (pageable, sort, query) => {
  const orderBy = sort.orderBy || '';
  const direction = sort.direction || '';
  return get(`${API_URL}/recipe/?page=${pageable.page}&size=${pageable.size}&query=${query}&orderBy=${orderBy}&direction=${direction}`);
};