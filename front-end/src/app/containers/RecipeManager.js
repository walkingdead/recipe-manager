import React, {Component} from "react";
import Pagination from "react-js-pagination";

import {CONFIG} from "../config/config";
import {getRecipesPageableAndSearch} from "../api/api"
import RecipeOverviewComponent from "./RecipeOverview";
import FilterAndSearch from "./FilterAndSearch";

export default class RecipeManager extends Component {

  constructor() {
    super();
    this.state = {
      items: {},
      query: '',
      activePage: 1,
      totalElements: 0,
      sort: {
        orderBy: 'name',
        direction: ''
      }
    }
  }

  componentWillMount() {
    const sort = this.state.sort;
    const query = this.state.query;
    const pageable = {page: 1, size: CONFIG.itemsPerPage};
    getRecipesPageableAndSearch(pageable, sort, query).then(response => {
          this.setState({
            items: response.data.content,
            totalElements: response.data.totalElements
          });
        }
    );
  }

  handlePageChange = pageNumber => {
    const sort = this.state.sort;
    const query = this.state.query;
    const pageable = {page: pageNumber, size: CONFIG.itemsPerPage};
    getRecipesPageableAndSearch(pageable, sort, query).then(response =>
        this.setState({
          items: response.data.content,
          activePage: pageNumber,
          totalElements: response.data.totalElements,
          sort: sort,
        })
    );
  };

  onSearchChange = query => {
    const sort = this.state.sort;
    const pageable = {page: 1, size: CONFIG.itemsPerPage};
    getRecipesPageableAndSearch(pageable, sort, query).then(response =>
        this.setState({
          items: response.data.content,
          totalElements: response.data.totalElements,
          activePage: 1,
          sort: {
            orderBy: 'name',
            direction: ''
          },
          query: query
        })
    );
  };

  onSortChange = sort => {
    const pageable = {page: this.state.activePage, size: CONFIG.itemsPerPage};
    getRecipesPageableAndSearch(pageable, sort, this.state.query).then(
        response =>
            this.setState({
              items: response.data.content,
              sort: {
                orderBy: sort.orderBy,
                direction: sort.direction
              },
            })
    );
  };

  render() {
    const filterAndSearch =
        <FilterAndSearch totalItems={this.state.totalElements}
                         onSearchChange={::this.onSearchChange}
                         onSortChange={::this.onSortChange}
                         orderBy={this.state.sort.orderBy}
                         query={this.state.query}
                         direction={this.state.sort.direction}/>;

    if (!this.state.items || !this.state.items.length) {
      return (<div>{filterAndSearch}</div>);
    }

    return (
        <div>
          {filterAndSearch}
          <div>
            {this.state.items.map(recipe =>
                <RecipeOverviewComponent recipe={recipe} key={recipe.id}/>
            )}
          </div>

          {this.state.totalElements > CONFIG.itemsPerPage && <div>
            <Pagination
                activePage={this.state.activePage}
                itemsCountPerPage={this.state.itemsPerPage}
                totalItemsCount={this.state.totalElements || 0}
                pageRangeDisplayed={10}
                onChange={::this.handlePageChange}
            />
          </div>}
        </div>
    );
  }
}