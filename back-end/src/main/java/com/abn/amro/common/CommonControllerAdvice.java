package com.abn.amro.common;


import static java.util.stream.Collectors.toList;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@ControllerAdvice
@RestController
public class CommonControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonControllerAdvice.class);

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorEntity wrongArgumentType(MethodArgumentTypeMismatchException e) {
        LOGGER.error("Wrong arguments: ", e.getMessage());
        return new ErrorEntity(String.format("Incorrect value [%s] for parameter '%s'", e.getValue(), e.getName()));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorEntity missedInputParameters(MissingServletRequestParameterException exception) {
        String errorMessage = String.format("Required [%s] parameter is missing", exception.getParameterName());
        LOGGER.error("Missing parameters ", errorMessage);
        return new ErrorEntity(errorMessage);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorEntity handleResourceNotFoundException(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        StringBuilder builder = new StringBuilder();
        violations.forEach(violation -> builder.append(violation.getMessage()).append("\n"));
        String message = builder.substring(0, builder.length() - 1);
        LOGGER.error("Constraint violations ", message);
        return new ErrorEntity(message);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorEntity invalidInputParameters(MethodArgumentNotValidException exception) {
        String errorMessage = String.format("Parameter %s %s", exception.getParameter().getParameterName(),
            exception.getBindingResult().getAllErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage).collect(toList()));
        LOGGER.error("Not valid arguments ", errorMessage);
        return new ErrorEntity(errorMessage);
    }


    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorEntity handleResourceNotFoundException(Throwable exception) {
        LOGGER.error("Unknown server error occurred", exception);
        return new ErrorEntity("Unknown server error");
    }
}