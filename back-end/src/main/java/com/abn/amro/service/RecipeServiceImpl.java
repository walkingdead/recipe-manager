package com.abn.amro.service;

import com.abn.amro.domain.Recipe;
import com.abn.amro.dto.FilterRequest;
import com.abn.amro.dto.PageResponse;
import com.abn.amro.mapper.RecipeMapper;
import com.abn.amro.repository.RecipeRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepository recipeRepository;

    public PageResponse<Recipe> getRecipes(FilterRequest filterRequest) {
        Sort sort = buildSort(filterRequest);
        Pageable pageable = PageRequest.of(filterRequest.getPage(), filterRequest.getSize(), sort);
        if (StringUtils.isBlank(filterRequest.getQuery())) {
            return RecipeMapper.mapToPageResponse(recipeRepository.findAll(pageable));
        }
        TextCriteria criteria = TextCriteria.forDefaultLanguage().matchingAny(filterRequest.getQuery());
        return RecipeMapper.mapToPageResponse(recipeRepository.findAllBy(criteria, pageable));
    }

    private static Sort buildSort(FilterRequest filterRequest) {
        if (StringUtils.isBlank(filterRequest.getOrderBy()) || filterRequest.getDirection() == null) {
            return Sort.unsorted();
        }
        return new Sort(filterRequest.getDirection(), filterRequest.getOrderBy());
    }
}