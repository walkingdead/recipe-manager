package com.abn.amro.mongobee;

import com.github.mongobee.Mongobee;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@Profile("!test")
public class MongobeeConfiguration {

    @Bean
    public MongoClient mongo(MongoProperties properties) {
        MongoClientURI uri = new MongoClientURI(getUrl(properties));
        return new MongoClient(uri);
    }

    @Bean
    public MongoTemplate mongoTemplate(MongoProperties properties) {
        return new MongoTemplate(mongo(properties), properties.getDatabase());
    }

    @Bean
    public Mongobee mongobee(MongoProperties properties) {
        String url = getUrl(properties);
        Mongobee runner = new Mongobee(url);
        runner.setDbName(properties.getDatabase());
        runner.setChangeLogsScanPackage("com.abn.amro.mongobee.changelogs");
        runner.setMongoTemplate(mongoTemplate(properties));
        return runner;
    }

    private String getUrl(MongoProperties properties) {
        return String.format("mongodb://%s:%s@%s:%d",
            properties.getUsername(), String.valueOf(properties.getPassword()), properties.getHost(),
            properties.getPort());
    }
}
