import React, {Component} from 'react'
import PropTypes from 'prop-types';

import '../stylesheets/search-box.sass'

export default class SearchBox extends Component {

  static propTypes = {
    totalItems: PropTypes.number.isRequired,
    onSearchChange: PropTypes.func.isRequired,
    query: PropTypes.string.isRequired
  };

  search(event) {
    event.preventDefault();
    const searchField = this.refs.searchField.value.trim();
    this.props.onSearchChange(searchField);
    this.refs.searchField.value = '';
  }

  render() {
    const search = this.props.query ? `For Search: [${this.props.query}]`
        : this.props.query;
    return (
        <form onSubmit={::this.search}>
          <div className="search-container">
            <span
                className="recipes-found">Items Found {this.props.totalItems} {search}</span>
            <input className="search-input" type="text" ref="searchField"
                   placeholder="Search"/>
            <button type="submit"
                    className="btn btn-info button-container">Search
            </button>
          </div>
        </form>
    )
  }
}