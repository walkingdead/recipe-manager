package com.abn.amro.it.service;


import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.core.Is.is;

import com.abn.amro.common.annotation.IntegrationTestRunner;
import com.abn.amro.domain.Recipe;
import com.abn.amro.dto.FilterRequest;
import com.abn.amro.dto.PageResponse;
import com.abn.amro.hepler.IntegrationTestHelper;
import com.abn.amro.repository.RecipeRepository;
import com.abn.amro.service.RecipeService;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTestRunner
@Import(EmbeddedMongoAutoConfiguration.class)
public class RecipeServiceTestIT {

    private static final int PAGE_SIZE = 10;

    @Autowired
    private RecipeService recipeService;

    @Autowired
    private RecipeRepository recipeRepository;

    private static List<Recipe> initialRecipes = IntegrationTestHelper.createRecipes();

    private static final long TOTAL_ITEMS = initialRecipes.size();

    @Before
    public void init() {
        recipeRepository.deleteAll();
        recipeRepository.saveAll(initialRecipes);
    }

    @Test
    public void getPageableResultsFirstPage() {
        FilterRequest filterRequest = FilterRequest.builder().page(0).size(PAGE_SIZE).build();
        PageResponse<Recipe> recipes = recipeService.getRecipes(filterRequest);

        assertThat("Recipes size", recipes.getContent(), hasSize(PAGE_SIZE));
        assertThat("Recipes page number", recipes.getPage(), is(1));
        assertThat("Recipes total pages", recipes.getTotalPages(), is(2));
        assertThat("Recipes total elements", recipes.getTotalElements(), is(TOTAL_ITEMS));
    }

    @Test
    public void getPageableResultsSecondPage() {
        FilterRequest filterRequest = FilterRequest.builder().page(1).size(15).build();
        PageResponse<Recipe> recipes = recipeService.getRecipes(filterRequest);

        assertThat("Recipes size", recipes.getContent(), hasSize(3));
        assertThat("Recipes page number", recipes.getPage(), is(2));
        assertThat("Recipes total pages", recipes.getTotalPages(), is(2));
        assertThat("Recipes total elements", recipes.getTotalElements(), is(TOTAL_ITEMS));
    }

    @Test
    public void getPageableResultsNonExistingPage() {
        FilterRequest filterRequest = FilterRequest.builder().page(2).size(PAGE_SIZE).build();
        PageResponse<Recipe> recipes = recipeService.getRecipes(filterRequest);

        assertThat("Recipes size", recipes.getContent(), empty());
        assertThat("Recipes page number", recipes.getPage(), is(3));
        assertThat("Recipes total pages", recipes.getTotalPages(), is(2));
        assertThat("Recipes total elements", recipes.getTotalElements(), is(TOTAL_ITEMS));
    }

    @Test
    public void getPageableResultsSearchQuery() {
        String query = "vinegar";
        String[] recipeNames = new String[]{"Shoepeg Corn Salad", "Bruschetta Salad", "Charred Vegetable Panzanella with Olive Oil Bread"};
        FilterRequest filterRequest = FilterRequest.builder().page(0).size(PAGE_SIZE).query(query).build();
        PageResponse<Recipe> recipes = recipeService.getRecipes(filterRequest);

        assertThat("Recipes size", recipes.getContent(), hasSize(3));
        assertThat("Recipes page number", recipes.getPage(), is(1));
        assertThat("Recipes total pages", recipes.getTotalPages(), is(1));
        assertThat("Recipes total elements", recipes.getTotalElements(), is(3L));

        List<String> foundRecipeNames = recipes.getContent().stream().map(Recipe::getName).collect(toList());

        assertThat("Recipes names", foundRecipeNames, containsInAnyOrder(recipeNames));
    }

    @Test
    public void getPageableResultsSearchQueryZeroResults() {
        FilterRequest filterRequest = FilterRequest.builder().page(0).size(PAGE_SIZE).query("shorts").build();
        PageResponse<Recipe> recipes = recipeService.getRecipes(filterRequest);

        assertThat("Recipes size", recipes.getContent(), empty());
        assertThat("Recipes page number", recipes.getPage(), is(1));
        assertThat("Recipes total pages", recipes.getTotalPages(), is(0));
        assertThat("Recipes total elements", recipes.getTotalElements(), is(0L));
    }

    @Test
    public void getPageableResultsOrderByNameAsc() {
        FilterRequest filterRequest = FilterRequest.builder().page(0).size((int) TOTAL_ITEMS).orderBy("name")
            .direction(Direction.ASC).build();
        PageResponse<Recipe> recipes = recipeService.getRecipes(filterRequest);

        assertThat("Recipes size", recipes.getContent(), hasSize((int) TOTAL_ITEMS));
        assertThat("Recipes page number", recipes.getPage(), is(1));
        assertThat("Recipes total pages", recipes.getTotalPages(), is(1));
        assertThat("Recipes total elements", recipes.getTotalElements(), is(TOTAL_ITEMS));

        List<String> initialRecipeNames = initialRecipes.stream().map(Recipe::getName).sorted().collect(toList());
        List<String> extractedRecipeNames = recipes.getContent().stream().map(Recipe::getName).collect(toList());

        assertThat("Recipes are sorted by name in ASC order", initialRecipeNames, equalTo(extractedRecipeNames));
    }

    @Test
    public void getPageableResultsOrderByNameDesc() {
        FilterRequest filterRequest = FilterRequest.builder().page(0).size((int) TOTAL_ITEMS).orderBy("name")
            .direction(Direction.DESC).build();
        PageResponse<Recipe> recipes = recipeService.getRecipes(filterRequest);

        assertThat("Recipes size", recipes.getContent(), hasSize((int) TOTAL_ITEMS));
        assertThat("Recipes page number", recipes.getPage(), is(1));
        assertThat("Recipes total pages", recipes.getTotalPages(), is(1));
        assertThat("Recipes total elements", recipes.getTotalElements(), is(TOTAL_ITEMS));

        List<String> initialRecipeNames = initialRecipes.stream().map(Recipe::getName)
            .sorted(Collections.reverseOrder()).collect(toList());
        List<String> extractedRecipeNames = recipes.getContent().stream().map(Recipe::getName).collect(toList());

        assertThat("Recipes are sorted by name in DESC order", initialRecipeNames, equalTo(extractedRecipeNames));
    }

}
