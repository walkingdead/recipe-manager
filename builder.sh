cd back-end && ./gradlew clean build && ./gradlew dockerBuild
cd ../front-end/src && npm run prod
cd ../ && docker build -t recipe-manager-client .