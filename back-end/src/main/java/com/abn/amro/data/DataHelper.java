package com.abn.amro.data;

import static com.abn.amro.Utils.randomDateTime;

import com.abn.amro.Utils;
import com.abn.amro.domain.Recipe;
import java.util.Arrays;
import java.util.List;
import lombok.experimental.UtilityClass;

@UtilityClass
public class DataHelper {

    public static List<Recipe> createRecipes() {

        Recipe charredVegetable = Recipe.builder()
            .name("Charred Vegetable Panzanella with Olive Oil Bread")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/5302770.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "6 slices Nature's Own® Perfectly Crafted Multigrain Bread; 1 medium zucchini, halved lengthwise;"
                    + "1 medium yellow squash, halved lengthwise; 1 medium Japanese eggplant, halved lengthwise;"
                    + "1 red onion, cut into 1/2-inch-thick slices; 5 tablespoons olive oil, divided; 1/2 teaspoon salt; "
                    + "1/4 teaspoon garlic powder; 1/4 teaspoon black pepper; 3 tablespoons red wine vinegar; "
                    + "3 cups arugula1 pound assorted color tomatoes, cut into 1-inch pieces; 1 cup finely shredded "
                    + "Parmesan cheese; 1/2 cup coarsely chopped fresh basilFinely shredded Parmesan cheese (optional)")
            .cookingInstructions(
                "Brush bread, zucchini, squash, eggplant, and red onion with 2 tablespoons olive oil and season with salt, garlic powder, and black pepper.\n"
                    + "Grill zucchini, yellow squash, eggplant, and red onion, covered, on grill rack, directly over medium-high heat for 6 to 8 minutes, or until tender, turning once halfway through grilling. Remove vegetables and set aside to cool slightly.\n"
                    + "Grill bread on grill rack directly over medium-high heat for 1 to 2 minutes or until toasted and lightly charred, turning once halfway through grilling. Remove bread and set aside.\n"
                    + "Cut grilled, cooled vegetables into 1-inch pieces. Transfer to a very large bowl. Toss with remaining 3 tablespoons olive oil and red wine vinegar. Add arugula, tomatoes, 1 cup Parmesan cheese, and basil and toss gently to combine.\n"
                    + "Cut grilled bread into 1-inch pieces and add to vegetable mixture; toss gently to combine.\n"
                    + "Serve immediately or let stand at room temperature for up to 30 minutes. To serve, garnish with additional Parmesan cheese, if desired.")
            .build();

        Recipe cheddarQuiche = Recipe.builder()
            .name("Cheddar Quiche")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/3912777.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "1 cup all-purpose baking mix; 1/4 teaspoon salt; 1/4 teaspoon ground black pepper; 1/3 cup milk; "
                    + "3 slices bacon, chopped; 1 small onion, chopped; 2 cups shredded Cheddar cheese; 4 eggs; "
                    + "1 teaspoon salt; 1/4 teaspoon hot pepper sauce; 1 (12 fluid ounce) can evaporated milk, heated")
            .cookingInstructions("Preheat oven to 400 degrees F (200 degrees C). Lightly grease a 9 inch pie pan.\n"
                + "In a medium bowl, mix together the baking mix, 1/4 teaspoon salt, and pepper. Gradually mix in the milk until moistened. Knead a few times on a floured board. Roll dough out to a 12 inch circle, and press into the greased pie pan. Fold edges, and flute.\n"
                + "Place bacon and onion in a large, deep skillet over medium-high heat, and cook until bacon is evenly brown. Drain, and crumble bacon. Sprinkle bacon, onion, and Cheddar cheese into the pie pan.\n"
                + "In a medium bowl, beat eggs with 1 teaspoon salt and hot pepper sauce, then slowly stir in hot evaporated milk. Pour into the pie shell.\n"
                + "Bake 5 minutes in the preheated oven, then reduce heat to 350 degrees F (175 degrees C). Continue baking 25 minutes, or until center is almost set. Do not over bake - the quiche will set as it cools.")
            .build();

        Recipe candiedKielbasa = Recipe.builder()
            .name("Candied Kielbasa")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/1132791.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "1 cup packed brown sugar; 1/2 cup ketchup; 1/4 cup prepared horseradish; 2 pounds kielbasa sausage, sliced thin")
            .cookingInstructions("In a slow cooker combine the sugar, ketchup and horseradish. "
                + "Add the sausage, and mix well. Cook on High until it starts to boil. "
                + "Reduce heat to Low, and cook until sauce thickens, about 45 minutes to 1 hour.")
            .build();

        Recipe chickenNepiev = Recipe.builder()
            .name("Chicken Nepiev")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/802802.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "1 tablespoon Italian seasoning; 1 tablespoon onion powder; 1 tablespoon garlic powder; 4 skinless, "
                    + "boneless chicken breast halves - pounded to 1/4 inch thickness; 4 tablespoons garlic flavored "
                    + "cream cheese spread; 1/4 cup garlic and herb seasoned dry bread crumbs; 1 tablespoon butter; "
                    + "1 tablespoon vegetable oilsalt and pepper to taste")
            .cookingInstructions(
                "In a cup or small bowl, stir together the Italian seasoning, onion powder and garlic powder. Sprinkle over both sides of the chicken. Place 1 tablespoon of cream cheese on the center of each piece, and spread slightly. Tuck in the sides, and roll up tightly. Secure with toothpicks.\n"
                    + "Place the bread crumbs on a plate or in a shallow bowl. Roll the chicken rolls in the bread crumbs to coat. Place on a plate, cover, and freeze for about 30 minutes.\n"
                    + "Preheat the oven to 400 degrees F (200 degrees C).\n"
                    + "Heat the butter and oil in a skillet over medium-high heat. Brown the chicken rolls on all sides, this should take about 5 minutes. Transfer the rolls to a baking dish.\n"
                    + "Bake for 20 minutes in the preheated oven, or until chicken is no longer pink and the juices run clear. Spoon drippings from the dish over the rolls before serving.")
            .build();

        Recipe breakfastGranolaCups = Recipe.builder()
            .name("Breakfast Granola Cups")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/2163496.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "cooking spray; 1/4 cup mashed banana; 1/4 cup honey; 1/2 teaspoon almond extract; 1 1/4 cups rolled oats;"
                    + "1/2 teaspoon ground cinnamon; 1/4 teaspoon salt; 3 cups yogurt")
            .cookingInstructions("Spray 6 muffin cups with cooking spray.\n"
                + "Whisk banana, honey, and almond extract together in a bowl until smooth. Whisk oats, cinnamon, and salt together in another bowl. Stir oat mixture into banana mixture until evenly mixed. Press mixture into the base and up the sides of the prepared muffin cups. Refrigerate until chilled and firm, 1 to 2 hours.\n"
                + "Preheat oven to 350 degrees F (175 degrees C).\n"
                + "Remove muffin cups from refrigerator and press each firmly into the base and sides of the muffin cups again.\n"
                + "Bake in the preheated oven until set and fragrant, 10 to 12 minutes. Press sides of granola cup into the muffin cup again with a spoon. Let cool for 20 minutes before removing from muffin cups. Fill each cooled cup with yogurt.")
            .build();

        Recipe spinachBrownies = Recipe.builder()
            .name("Spinach Brownies")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/3239956.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "1 (10 ounce) package spinach, rinsed and chopped; 1 cup all-purpose flour; 1 teaspoon salt; "
                    + "1 teaspoon baking powder; 2 eggs; 1 cup milkl; 1/2 cup butter, melted;"
                    + "1 onion, chopped; 1 (8 ounce) package shredded mozzarella cheeseAdd all ingredients to list")
            .cookingInstructions(
                "Preheat oven to 375 degrees F (190 degrees C). Lightly grease a 9x13 inch baking dish.\n"
                    + "Place spinach in a medium saucepan with enough water to cover. Bring to a boil. Lower heat to simmer and cook until spinach is limp, about 3 minutes. Remove from heat, drain, and set aside.\n"
                    + "In a large bowl, mix flour, salt and baking powder. Stir in eggs, milk and butter. Mix in spinach, onion and mozzarella cheese.\n"
                    + "Transfer the mixture to the prepared baking dish. Bake in the preheated oven 30 to 35 minutes, or until a toothpick inserted in the center comes out clean. Cool before serving.")
            .build();

        Recipe hamburgerCasserole = Recipe.builder()
            .name("Hamburger Casserole")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/1427057.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "1 pound ground beef1 onion, chopped; 1 stalk celery, chopped; 8 ounces egg noodles; 1 (15 ounce) can chili; "
                    + "1 (14.5 ounce) can peeled and diced tomatoes; 1 (15 ounce) can whole kernel corn, drained; "
                    + "1/4 cup salsa; 1 (1 ounce) package taco seasoning mix")
            .cookingInstructions(
                "Preheat oven to 250 degrees F (120 degrees C).\n"
                    + "In a large skillet over medium heat, combine the ground beef, onion and celery and saute for 10 minutes, or until the meat is browned and the onion is tender. Drain the fat and set aside.\n"
                    + "In a separate saucepan, cook noodles according to package directions. When cooked, drain the water and stir in the meat mixture, chili, tomatoes, corn, taco sauce and taco seasoning mix. Mix well and place entire mixture into a 10x15 baking dish.\n"
                    + "Bake at 250 degrees F (120 degrees C) for 20 minutes, or until thoroughly heated.")
            .build();

        Recipe blueberryDropCookies = Recipe.builder()
            .name("Blueberry Drop Cookies")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/278858.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "2 cups all-purpose flour; 2 teaspoons baking powder; 1/2 teaspoon salt; 1/2 cup shortening; "
                    + "1/4 cup milk; 1 egg; 1 cup white sugar; 1 teaspoon almond extract; 1 1/2 teaspoons lemon zest;"
                    + "1 cup fresh blueberries")
            .cookingInstructions(
                "In a large mixing bowl, cream the shortening, sugar, egg, milk, almond extract and lemon zest. Mix well after the addition of each ingredient. Combine the flour, baking powder and salt; blend into the sugar mixture. Fold in the blueberries. Cover and chill for 4 hours.\n"
                    + "Preheat oven to 375 degrees F. Drop dough by teaspoonfuls onto ungreased cookie sheets, about 1 1/2 inches apart.\n"
                    + "Bake 12 to 15 minutes in the preheated oven. Let the cookies cool on the baking sheets for a few minutes before transferring to wire racks to cool completely.")
            .build();

        Recipe shoepegCornSalad = Recipe.builder()
            .name("Shoepeg Corn Salad")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/1166630.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "1/2 cup mayonnaise; 3 small green onions, thinly sliced; 2 tablespoons white wine vinegar; "
                    + "2 tablespoons minced pickled jalapeno peppers; 2 tablespoons minced fresh parsley; 1 tablespoon "
                    + "light olive oilsalt and ground black pepper to taste; 2 (11 ounce) cans shoepeg corn, "
                    + "rinsed and drained; 1 cup halved grape tomatoes")
            .cookingInstructions(
                "Whisk mayonnaise, green onions, vinegar, jalapeno peppers, parsley, and olive oil together in a bowl until smooth; season with salt and pepper.\n"
                    + "Gently stir corn and tomatoes into the dressing to coat. Transfer salad to a serving dish, cover with plastic wrap, and refrigerate at least 2 hours.")
            .build();

        Recipe slowCookerBakedPotatoes = Recipe.builder()
            .name("Slow Cooker Baked Potatoes")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/1009478.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "4 baking potatoes, well scrubbed;1 tablespoon extra virgin olive oilkosher salt to taste; "
                    + "4 sheets aluminum foil")
            .cookingInstructions(
                "Prick the potatoes with a fork several times, then rub potatoes with olive oil, sprinkle with salt, and wrap tightly in foil. Place the potatoes into a slow cooker, cover, and cook on High for 4 1/2 to 5 hours, or on Low for 7 1/2 to 8 hours until tender.")
            .build();

        Recipe lemonLush = Recipe.builder()
            .name("Lemon Lush")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/3577434.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "Preheat oven to 350 degrees F (175 degrees C). In a medium bowl, combine the flour and butter using a pastry cutter until a ball forms. Press into the bottom of a 9x13 inch baking dish.\n"
                    + "Bake for 25 minutes in the preheated oven, or until lightly golden. Remove from oven and allow to cool completely.\n"
                    + "In a medium bowl, beat the cream cheese and sugar together until smooth and well blended. Spread evenly over the cooled crust. In another bowl, whisk together the lemon pudding mix and milk for 3 to 5 minutes. Spread over the cream cheese layer. Chill until set, then top with whipped topping.")
            .cookingInstructions(
                "")
            .build();

        Recipe zucchiniPie = Recipe.builder()
            .name("Zucchini Pie")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/681137.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "3 cups zucchini, diced; 1 onion, chopped; 4 eggs, beaten; 1 cup buttermilk baking mix; 1/2 cup vegetable"
                    + " oil; 1/2 cup grated Parmesan cheese; 1/2 teaspoon dried marjoram; 1 teaspoon chopped parsley, "
                    + "or to tasteground black pepper to taste")
            .cookingInstructions(
                "Preheat oven to 350 degrees F (175 degrees C). Grease a 10x6-inch pan or a 12-inch pie plate.\n"
                    + "In a medium mixing bowl, combine zucchini, onion, eggs, buttermilk baking mix, vegetable oil, Parmesan cheese, marjoram, parsley and pepper; mix well. Spread into the prepared baking dish.\n"
                    + "Bake for 30 minutes, or until lightly brown.")
            .build();

        Recipe bruschettaSalad = Recipe.builder()
            .name("Bruschetta Salad")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/2332714.jpg")
            .isVegetarian(false)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "6 Roma tomatoes, sliced into rounds; 1/2 pound mozzarella cheese, cut into bite-size cubes;"
                    + "1 cup crushed garlic-flavored bagel chips; 1/2 cup torn fresh basil leaves;"
                    + "1/2 red onion, chopped; 1/4 cup light olive oil; 3 tablespoons red wine vinegar; 2 large cloves "
                    + "garlic, minced; 1 tablespoon dried basilsalt and ground black pepper to taste")
            .cookingInstructions(
                "Combine tomatoes, mozzarella cheese, bagel chips, basil, red onion, olive oil, red wine vinegar, garlic,"
                    + " basil, salt, and black pepper together in a bowl; toss until evenly combined."
                    + " Refrigerate until chilled, 15 to 30 minutes.")
            .build();

        Recipe veganTomatoSoup = Recipe.builder()
            .name("Vegan Tomato Soup")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/5261486.jpg")
            .isVegetarian(true)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "2 tablespoons extra-virgin olive oil; 1 onion, chopped; 4 cloves garlic, minced; 4 tomatoes, chopped; "
                    + "3 1/2 cups cherry tomatoes, halved; 3/4 cup vegetable broth ;2 bay leaves ;2 sprigs fresh basil,"
                    + " divided")
            .cookingInstructions(
                "Heat olive oil in a pot over low heat and cook onion until soft and translucent. Add garlic and cook until fragrant, about 1 minute. Increase heat to medium, add all tomatoes, and cook until they start to break down, about 5 minutes. Stir occasionally. Add vegetable broth, bay leaves, and 1 sprig of basil. Bring to a boil, reduce heat, and simmer until tomatoes have broken down and soup starts to thicken, about 30 minutes.\n"
                    + "Remove soup from heat and cool slightly. Remove bay leaves and basil.\n"
                    + "Puree tomato soup with an immersion blender until smooth. Reheat soup before serving and garnish with basil leaves.")
            .build();

        Recipe guacamole = Recipe.builder()
            .name("Guacamole")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/811729.jpg")
            .isVegetarian(true)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "3 avocados - peeled, pitted, and mashed1 lime, juiced; 1 teaspoon salt; 1/2 cup diced onion; 3 tablespoons"
                    + " chopped fresh cilantro; 2 roma (plum) tomatoes, diced; 1 teaspoon minced garlicl 1 pinch ground "
                    + "cayenne pepper (optional)")
            .cookingInstructions(
                "In a medium bowl, mash together the avocados, lime juice, and salt. Mix in onion, cilantro, tomatoes, "
                    + "and garlic. Stir in cayenne pepper. Refrigerate 1 hour for best flavor, or serve immediately.")
            .build();

        Recipe spicyBeanSalsa = Recipe.builder()
            .name("Spicy Bean Salsa")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/792466.jpg")
            .isVegetarian(true)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "1 (15 ounce) can black-eyed peas1 (15 ounce) can black beans, rinsed and drained;"
                    + "1 (15 ounce) can whole kernel corn, drained; 1/2 cup chopped onion; 1/2 cup chopped green bell "
                    + "pepper; 1 (4 ounce) can diced jalapeno peppers ;1 (14.5 ounce) can diced tomatoes, drained; "
                    + "1 cup Italian-style salad dressing; 1/2 teaspoon garlic salt")
            .cookingInstructions(
                "In a medium bowl, combine black-eyed peas, black beans, corn, onion, green bell pepper, jalapeno peppers"
                    + " and tomatoes. Season with Italian-style salad dressing and garlic salt; mix well. "
                    + "Cover, and refrigerate overnight to blend flavors.")
            .build();

        Recipe realHummus = Recipe.builder()
            .name("Real Hummus")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/4524208.jpg")
            .isVegetarian(true)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "1 clove garlic; 1 (19 ounce) can garbanzo beans, half the liquid reserved; 4 tablespoons lemon juice;"
                    + "2 tablespoons tahini; 1 clove garlic, chopped; 1 teaspoon saltblack pepper to taste;"
                    + "2 tablespoons olive oil")
            .cookingInstructions(
                "In a blender, chop the garlic. Pour garbanzo beans into the blender, reserving about a tablespoon for garnish. Add reserved liquid, lemon juice, tahini, and salt to the blender. Blend until creamy and well mixed.\n"
                    + "Transfer the mixture to a medium serving bowl. Sprinkle with pepper and pour olive oil over the top. Garnish with reserved garbanzo beans.")
            .build();

        Recipe churros = Recipe.builder()
            .name("Churros")
            .createdAt(randomDateTime())
            .imageUrl("https://images.media-allrecipes.com/userphotos/300x300/392145.jpg")
            .isVegetarian(true)
            .personsPerDish(Utils.randomInt())
            .ingredientList(
                "1 cup water; 2 1/2 tablespoons white sugar; 1/2 teaspoon salt; 2 tablespoons vegetable oil;"
                    + "1 cup all-purpose flour; 2 quarts oil for frying; 1/2 cup white sugar, or to taste;"
                    + "1 teaspoon ground cinnamon")
            .cookingInstructions(
                "In a small saucepan over medium heat, combine water, 2 1/2 tablespoons sugar, salt and 2 tablespoons vegetable oil. Bring to a boil and remove from heat. Stir in flour until mixture forms a ball.\n"
                    + "Heat oil for frying in deep-fryer or deep skillet to 375 degrees F (190 degrees C). Pipe strips of dough into hot oil using a pastry bag. Fry until golden; drain on paper towels.\n"
                    + "Combine 1/2 cup sugar and cinnamon. Roll drained churros in cinnamon and sugar mixture")
            .build();

        return Arrays.asList(charredVegetable, cheddarQuiche, veganTomatoSoup, candiedKielbasa, chickenNepiev,
            breakfastGranolaCups, guacamole, spinachBrownies, hamburgerCasserole, spicyBeanSalsa, blueberryDropCookies,
            shoepegCornSalad, realHummus, slowCookerBakedPotatoes, lemonLush, zucchiniPie, churros, bruschettaSalad
        );
    }
}
