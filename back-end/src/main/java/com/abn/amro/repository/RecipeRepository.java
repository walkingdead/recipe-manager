package com.abn.amro.repository;

import com.abn.amro.domain.Recipe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecipeRepository extends PagingAndSortingRepository<Recipe, String> {

    Page<Recipe> findAllBy(TextCriteria criteria, Pageable page);
}
