import React from "react";
import SearchBox from "../components/SearchBox";
import PropTypes from "prop-types";
import SortBox from "./SortBox";

import '../stylesheets/filter-and-search.sass'

const FilterAndSearch = props => {
  return (
      <div className="panel panel-default">
        <div className="panel-body filter-and-search">
            <span>
            <SortBox onSortChange={props.onSortChange}
                     orderBy={props.orderBy}
                     direction={props.direction}/>
              </span>
          <SearchBox totalItems={props.totalItems}
                     query={props.query}
                     onSearchChange={props.onSearchChange}/>
        </div>
      </div>
  )
};

export default FilterAndSearch;

FilterAndSearch.propTypes = {
  totalItems: PropTypes.number.isRequired,
  orderBy: PropTypes.string.isRequired,
  query: PropTypes.string.isRequired,
  direction: PropTypes.string.isRequired,
  onSortChange: PropTypes.func.isRequired,
  onSearchChange: PropTypes.func.isRequired,
};
