package com.abn.amro.mapper;

import com.abn.amro.dto.PageResponse;
import lombok.experimental.UtilityClass;
import org.springframework.data.domain.Page;

@UtilityClass
public class RecipeMapper {

    public static <T> PageResponse<T> mapToPageResponse(Page<T> page) {
        return PageResponse.<T>builder()
            .content(page.getContent())
            .totalElements(page.getTotalElements())
            .totalPages(page.getTotalPages())
            .size(page.getSize())
            .page(page.getNumber() + 1)
            .build();
    }
}
