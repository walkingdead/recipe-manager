package com.abn.amro.hepler;

import com.abn.amro.data.DataHelper;
import com.abn.amro.domain.Recipe;
import java.util.List;


public class IntegrationTestHelper {

    public static List<Recipe> createRecipes() {
        return DataHelper.createRecipes();
    }
}
