package com.abn.amro.unit.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.abn.amro.hepler.TestHelper;
import com.abn.amro.rest.RecipeController;
import com.abn.amro.service.RecipeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(RecipeController.class)
public class RecipeControllerTest {


    private static final String ROOT_PATH = "/recipe";

    private static final String PAGE_PARAM_NAME = "page";
    private static final String PAGE_PARAM_SIZE_NAME = "size";
    private static final String QUERY_PARAM_NAME = "query";
    private static final String SORT_FIELD_PARAM_NAME = "orderBy";
    private static final String SORT_DIRECTION_PARAM_NAME = "direction";

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @MockBean
    private RecipeService recipeService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
            .webAppContextSetup(context)
            .build();
    }

    @Test
    public void testGetFewResults() throws Exception {

        when(recipeService.getRecipes(any())).thenReturn(TestHelper.generateRecipeResponse());
        mockMvc.perform(
            get(ROOT_PATH)
                .param(PAGE_PARAM_SIZE_NAME, "1")
                .param(PAGE_PARAM_NAME, "10"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.content.size()").value(2))
            .andExpect(jsonPath("$.content[0].name").value(TestHelper.RECIPE_NAMES.get(0)))
            .andExpect(jsonPath("$.content[1].name").value(TestHelper.RECIPE_NAMES.get(1)))
            .andExpect(jsonPath("$.totalElements").value(2))
            .andExpect(jsonPath("$.totalPages").value(1));
    }

    @Test
    public void testGetEmptyResults() throws Exception {

        when(recipeService.getRecipes(any())).thenReturn(TestHelper.generateEmptyRecipeResponse());
        mockMvc.perform(
            get(ROOT_PATH)
                .param(PAGE_PARAM_SIZE_NAME, "1")
                .param(PAGE_PARAM_NAME, "10")
                .param(QUERY_PARAM_NAME, "smth"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.content.size()").value(0))
            .andExpect(jsonPath("$.totalElements").value(0))
            .andExpect(jsonPath("$.totalPages").value(0));
    }

    @Test
    public void testIncorrectPageSizeValue() throws Exception {
        mockMvc.perform(
            get(ROOT_PATH)
                .param(PAGE_PARAM_SIZE_NAME, "0")
                .param(PAGE_PARAM_NAME, "1"))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("{\"message\":\"Min page size value is 1\"}"));
    }

    @Test
    public void testIncorrectPageValue() throws Exception {
        mockMvc.perform(
            get(ROOT_PATH)
                .param(PAGE_PARAM_SIZE_NAME, "1")
                .param(PAGE_PARAM_NAME, "0"))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("{\"message\":\"Min page value is 1\"}"));
    }

    @Test
    public void testMissedPageSizeValue() throws Exception {
        mockMvc.perform(
            get(ROOT_PATH)
                .param(PAGE_PARAM_NAME, "1"))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("{\"message\":\"Required [size] parameter is missing\"}"));
    }

    @Test
    public void testMissedPageValue() throws Exception {
        mockMvc.perform(
            get(ROOT_PATH)
                .param(PAGE_PARAM_SIZE_NAME, "1"))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("{\"message\":\"Required [page] parameter is missing\"}"));
    }

    @Test
    public void testIncorrectDirectionOrderValue() throws Exception {
        mockMvc.perform(
            get(ROOT_PATH)
                .param(PAGE_PARAM_SIZE_NAME, "0")
                .param(PAGE_PARAM_NAME, "10")
                .param(SORT_DIRECTION_PARAM_NAME, "smth"))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("{\"message\":\"Incorrect value [smth] for parameter 'direction'\"}"));
    }

}
