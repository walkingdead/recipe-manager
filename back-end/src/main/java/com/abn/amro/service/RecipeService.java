package com.abn.amro.service;

import com.abn.amro.domain.Recipe;
import com.abn.amro.dto.FilterRequest;
import com.abn.amro.dto.PageResponse;

public interface RecipeService {

    PageResponse<Recipe> getRecipes(FilterRequest filterRequest);
}
