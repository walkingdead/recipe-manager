package com.abn.amro.service;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.abn.amro.domain.Recipe;
import com.abn.amro.dto.FilterRequest;
import com.abn.amro.dto.PageResponse;
import com.abn.amro.hepler.TestHelper;
import com.abn.amro.repository.RecipeRepository;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Term;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class RecipeServiceTest {

    @Mock
    private RecipeRepository recipeRepository;

    private RecipeService recipeService;

    @Before
    public void init() {
        recipeService = new RecipeServiceImpl(recipeRepository);
    }

    @Test
    public void testGetOnlyPageable() {
        FilterRequest filterRequest = FilterRequest.builder().size(10).page(0).build();
        ArgumentCaptor<Pageable> captor = ArgumentCaptor.forClass(Pageable.class);

        when(recipeRepository.findAll(any(Pageable.class))).thenReturn(TestHelper.generatePageRecipe());

        PageResponse<Recipe> recipes = recipeService.getRecipes(filterRequest);

        verify(recipeRepository).findAll(captor.capture());
        verify(recipeRepository, never()).findAllBy(any(), any());

        Pageable pageable = captor.getValue();

        assertThat("Page number", pageable.getPageNumber(), is(0));
        assertThat("Page size", pageable.getPageSize(), is(10));
        assertThat("Page sort",  pageable.getSort(), is(Sort.unsorted()));

        assertThat("recipes size", recipes.getContent(), hasSize(2));

        List<String> recipeNames = recipes.getContent().stream().map(Recipe::getName).collect(toList());

        assertThat("Recipe names", recipeNames, containsInAnyOrder(TestHelper.RECIPE_NAMES.toArray()));
        assertThat("Total elements", recipes.getTotalElements(), is(2L));
        assertThat("Total pages", recipes.getTotalPages(), is(1));
    }

    @Test
    public void testGetPageableAndSearchable() {
        FilterRequest filterRequest = FilterRequest.builder().size(10).page(0).query("meal").build();
        ArgumentCaptor<TextCriteria> captor = ArgumentCaptor.forClass(TextCriteria.class);

        when(recipeRepository.findAllBy(any(), any())).thenReturn(TestHelper.generatePageRecipe());

        PageResponse<Recipe> recipes = recipeService.getRecipes(filterRequest);

        verify(recipeRepository, never()).findAll(any(Pageable.class));
        verify(recipeRepository).findAllBy(captor.capture(), any());

        TextCriteria textCriteria = captor.getValue();

        assertThat("Text criteria", getTextCriteriaValue(textCriteria), is("meal"));

        assertThat("recipes size", recipes.getContent(), hasSize(2));

        List<String> recipeNames = recipes.getContent().stream().map(Recipe::getName).collect(toList());

        assertThat("Recipe names", recipeNames, containsInAnyOrder(TestHelper.RECIPE_NAMES.toArray()));
        assertThat("Total elements", recipes.getTotalElements(), is(2L));
        assertThat("Total pages", recipes.getTotalPages(), is(1));
    }

    @SuppressWarnings("unchecked")
    private String getTextCriteriaValue(TextCriteria textCriteria){
        List<Term> terms = (List<Term>) ReflectionTestUtils.getField(textCriteria, "terms");
        return terms.get(0).getFormatted();
    }
}
