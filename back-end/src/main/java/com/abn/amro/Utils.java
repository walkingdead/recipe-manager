package com.abn.amro;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Utils {

    public static int randomInt(){
        Random random = new Random();
        return randomInt(3);
    }

    public static int randomInt(int max){
        Random random = new Random();
        return random.nextInt(max) + 1;
    }

    public static LocalDateTime randomDateTime() {
        return LocalDateTime.now().minus(Utils.randomInt(30), ChronoUnit.DAYS);
    }
}
