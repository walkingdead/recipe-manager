# Warning
After cloning repository gradlew in unix system contains windows line ending \r.
To fix problem go to `back-end` and execute `dos2unix gradlew`. After this problem is fixed.

## How to build

#Warning
 After cloning project real disaster happened.
 `gradlew` and `build.sh` script contains windows line ending.
 To fix issue execute following commands from rot project directory
 
 `dos2unix builder.sh` and `dos2unix back-end/gradlew`

Prerequisites:

    `docker, docker-compose`
    
All commands are executed from the root of the project. 

Before build you have to go to `/front-end/package.json`

  and change in API_URL `192.168.0.12` to your host IP. Front-end should know how to access back-end
  from docker container

`"prod": "npm install && API_URL=http://192.168.0.12:8082 webpack --config webpack.prod.config"`

If you are on linux/mac machine execute:
 
    `./build.sh`
    
If you are on windows machine execute:
 
    `build.bat`
    
 Notes: only `./build sh` was actually tested`
 
 After this operation two docker images `recipe-manager-server` and `recipe-manager-server`
 will be installed on your local machine
 
After successful build you have to go to `docker-compose.yml` and change
  
  `- SPRING_DATA_MONGODB_HOST=192.168.0.12` to your local host IP. Back-end should know where mongo db lives.

Finally you can execute command

  `docker-compose up`

and go to 
  
  `http://localhost:3000`