const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: [
    './app/index.js'
  ],
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: 'bundle.js'
  },
  cache: true,

  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!autoprefixer-loader!',
      },
      {
        test: /\.sass$/,
        loader: 'style-loader!css-loader!autoprefixer-loader!sass',
      },
      {
        test: /\.less$/,
        loader: 'style-loader!css-loader!autoprefixer-loader!less',
      },
      {
        test: /\.js?$/,
        loaders: ['react-hot/webpack', 'babel'],
        include: path.join(__dirname, 'app')
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000'
      }
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env':{
        NODE_ENV: JSON.stringify('production'),
        API_URL: JSON.stringify(process.env.API_URL)
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: false,
      mangle: false
    })
  ],
  resolve: {
    extensions: ['', '.js', '.json'],
  },
};
