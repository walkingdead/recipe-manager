package com.abn.amro.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilterRequest {

    private String orderBy;
    private Direction direction;
    private int size;
    private int page;
    private String query;
}
