import React, {Component} from 'react';
import RecipeManager from "./RecipeManager";

class App extends Component {
  render() {
    return (
        <div className="container-fluid">
         <RecipeManager/>
        </div>
    );
  }
}

export default App;