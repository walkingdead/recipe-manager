import React from 'react'
import PropTypes from 'prop-types';

import "../stylesheets/sorting-arrows.sass";

const SortingArrows = (props) => {

  const ASC = 'ASC';
  const DESC = 'DESC';

  const onSortChange = (direction) => () =>
      props.onSortChange({
        orderBy: props.property,
        direction: direction
      });

  const isPropertyActive = () => props.activeProperty === props.property;

  return (
      <span>
                {(props.direction === '' || !isPropertyActive()) &&
                <span>
                    <span className="glyphicon glyphicon-triangle-bottom"
                          onClick={onSortChange(DESC)}/>
                    <span className="glyphicon glyphicon-triangle-top"
                          onClick={onSortChange(ASC)}/>
                </span>}
        {props.direction === ASC && isPropertyActive() &&
        <span className="glyphicon glyphicon-triangle-top arrow-icon"
              onClick={onSortChange(DESC)}/>
        }
        {props.direction === DESC && isPropertyActive() &&
        <span className="glyphicon glyphicon-triangle-bottom arrow-icon"
              onClick={onSortChange(ASC)}/>
        }
            </span>
  )

};

SortingArrows.propTypes = {
  property: PropTypes.string.isRequired,
  activeProperty: PropTypes.string.isRequired,
  direction: PropTypes.string.isRequired,
  onSortChange: PropTypes.func.isRequired,
};

export default SortingArrows