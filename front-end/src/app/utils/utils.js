export function parseDate(date) {
  return `${date.slice(0,10)} ${date.slice(11, 19)}`
}