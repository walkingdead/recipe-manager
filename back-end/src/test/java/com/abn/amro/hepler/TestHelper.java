package com.abn.amro.hepler;


import static com.abn.amro.Utils.randomDateTime;
import static com.abn.amro.Utils.randomInt;
import static java.util.Collections.emptyList;

import com.abn.amro.Utils;
import com.abn.amro.domain.Recipe;
import com.abn.amro.dto.PageResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@UtilityClass
public class TestHelper {

    public static final List<String> RECIPE_NAMES = Arrays.asList("Lemon Lush", "Chicken Nepiev");

    public PageResponse<Recipe> generateRecipeResponse() {
        List<Recipe> recipes = buildRecipes();
        return PageResponse.<Recipe>builder().content(recipes).page(recipes.size() / 10 + 1).size(10)
            .totalPages(1).totalElements(recipes.size()).build();
    }

    public Page<Recipe> generatePageRecipe() {
        List<Recipe> recipes = buildRecipes();
        Pageable pageable = PageRequest.of(0, 10);
        return new PageImpl<>(recipes, pageable, recipes.size());
    }

    public Page<Recipe> generateEmptyPageRecipe() {
        List<Recipe> recipes = Collections.emptyList();
        Pageable pageable = PageRequest.of(0, recipes.size());
        return new PageImpl<>(recipes, pageable, recipes.size());
    }

    public PageResponse<Recipe> generateEmptyRecipeResponse() {
        return PageResponse.<Recipe>builder().content(emptyList()).page(0).size(10).totalPages(0).totalElements(0)
            .build();
    }

    private static List<Recipe> buildRecipes() {
        return RECIPE_NAMES.stream().map(name ->
            Recipe.builder()
                .name(name)
                .createdAt(randomDateTime())
                .imageUrl(
                    String.format("https://images.media-allrecipes.com/userphotos/300x300/%d.jpg", randomInt(100_000)))
                .isVegetarian(false)
                .personsPerDish(Utils.randomInt())
                .ingredientList(RandomStringUtils.random(100))
                .cookingInstructions(RandomStringUtils.random(200))
                .build()).collect(Collectors.toList());
    }

}
