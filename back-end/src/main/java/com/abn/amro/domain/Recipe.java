package com.abn.amro.domain;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Recipe {

    @Id
    private  String id;

    @TextIndexed(weight = 3)
    private String name;

    private String imageUrl;

    private Boolean isVegetarian;

    private Integer personsPerDish;

    private LocalDateTime createdAt;

    @TextIndexed(weight = 2)
    private String ingredientList;

    @TextIndexed(weight = 2)
    private String cookingInstructions;
}
