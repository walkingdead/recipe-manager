const express = require('express');
const app = express();
const compression = require('compression');

app.use(compression());
app.use(express.static(__dirname + ''));

app.all("/*", function(req, res) {
    res.sendFile('index.html', { root: __dirname + '' });
});

const server = app.listen(process.env.PORT0 || 3000, function () {
   const host = server.address().address;
   const port = server.address().port;
    console.log('Server port ' + process.env.PORT0);
    console.log('Example app listening at http://%s:%s', host, port)
});