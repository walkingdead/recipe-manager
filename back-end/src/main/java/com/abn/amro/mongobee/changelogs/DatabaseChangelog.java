package com.abn.amro.mongobee.changelogs;

import com.abn.amro.data.DataHelper;
import com.abn.amro.domain.Recipe;
import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import java.util.List;
import org.springframework.data.mongodb.core.MongoTemplate;

@ChangeLog
public class DatabaseChangelog {

    @ChangeSet(order = "001", id = "fillDatabase", author = "anonymous")
    public void fillDatabase(MongoTemplate template) {
        List<Recipe> recipes = DataHelper.createRecipes();
        recipes.forEach(recipe -> template.save(recipe, "recipe"));
    }
}