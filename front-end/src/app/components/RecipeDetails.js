import React from "react";
import PropTypes from 'prop-types';

import '../stylesheets/recipe-details-component.sass'
import {parseDate} from "../utils/utils";

const RecipeDetails = props => {

  return (
      <div className="collapse" id={props.id}>
        <div className="recipe-block">
          <img src={props.imageUrl}/>
          <div className="panel panel-default recipe-details">
            <div className="panel-body">
              <div><strong>Dish:</strong> {props.name}</div>
              <div><strong>Vegetarian:</strong> {props.isVegetarian
                  ? 'Yes' : 'No'} </div>
              <div><strong>Persons Per Dish:</strong> {props.personsPerDish}</div>
              <div><strong>Created:</strong> {parseDate(props.createdAt)}
              </div>
              <div><strong>Ingredients:</strong> {props.ingredientList}
              </div>
              <div><strong>Cooking
                instructions:</strong> {props.cookingInstructions}</div>
            </div>
          </div>
        </div>
      </div>
  )
};

RecipeDetails.propTypes = {
  id: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  personsPerDish: PropTypes.number.isRequired,
  ingredientList: PropTypes.string.isRequired,
  isVegetarian: PropTypes.bool.isRequired,
  cookingInstructions: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired
};

export default RecipeDetails