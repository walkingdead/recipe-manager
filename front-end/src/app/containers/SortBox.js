import React from 'react'
import SortingArrows from "../components/SortingArrows";
import PropTypes from 'prop-types';

import '../stylesheets/sort-box.sass'

const SortBox = props => {

  return (
      <div>
        <span className="sort-box">Sort By: Name
        <SortingArrows direction={props.direction}
                       activeProperty={props.orderBy}
                       property={props.orderBy}
                       onSortChange={::props.onSortChange}/>
        </span>
      </div>
  )
};

SortBox.propTypes = {
  orderBy: PropTypes.string.isRequired,
  direction: PropTypes.string.isRequired,
  onSortChange: PropTypes.func.isRequired,
};

export default SortBox


