package com.abn.amro.it.controller;


import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.core.Is.is;

import com.abn.amro.common.annotation.FunctionalTestRunner;
import com.abn.amro.domain.Recipe;
import com.abn.amro.dto.PageResponse;
import com.abn.amro.hepler.IntegrationTestHelper;
import com.abn.amro.repository.RecipeRepository;
import java.util.List;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FunctionalTestRunner
@Import(EmbeddedMongoAutoConfiguration.class)
public class RecipeControllerTestIT {


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RecipeRepository recipeRepository;

    @Value("${local.server.port}")
    private String port;

    private static List<Recipe> initialRecipes = IntegrationTestHelper.createRecipes();

    @Before
    public void init() {
        recipeRepository.deleteAll();
        recipeRepository.saveAll(initialRecipes);
    }

    @Test
    public void getRecipesByQuery() {
        String[] recipeNames = new String[]{"Vegan Tomato Soup", "Bruschetta Salad", "Charred Vegetable Panzanella with Olive Oil Bread"};
        ResponseEntity<PageResponse<Recipe>> response = restTemplate.exchange(
            "http://localhost:{port}/recipe?page={page}&size={size}&query={query}",
            HttpMethod.GET, null, new ParameterizedTypeReference<PageResponse<Recipe>>() {
            }, port, 1, 10, "basil");
        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        PageResponse<Recipe> pageResponse = response.getBody();

        assertThat("Page response is present", pageResponse, notNullValue());

        assertThat("Recipes size", pageResponse.getContent(), hasSize(3));
        assertThat("Recipes page number", pageResponse.getPage(), is(1));
        assertThat("Recipes total pages", pageResponse.getTotalPages(), is(1));
        assertThat("Recipes total elements", pageResponse.getTotalElements(), is(3L));

        List<String> foundRecipeNames = pageResponse.getContent().stream().map(Recipe::getName).collect(toList());

        assertThat("Recipes names", foundRecipeNames, containsInAnyOrder(recipeNames));
    }
}
