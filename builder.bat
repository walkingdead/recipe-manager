cd back-end && gradlew.bat clean build && gradlew.bat -q dockerBuild
cd ../front-end/src && npm run prod
cd ../ && docker build -t recipe-manager-client .