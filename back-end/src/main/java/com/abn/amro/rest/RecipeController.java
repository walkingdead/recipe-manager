package com.abn.amro.rest;


import com.abn.amro.domain.Recipe;
import com.abn.amro.dto.FilterRequest;
import com.abn.amro.dto.PageResponse;
import com.abn.amro.service.RecipeService;
import javax.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("recipe")
@RequiredArgsConstructor
@Validated
public class RecipeController {

    private final RecipeService recipeService;

    @GetMapping
    @CrossOrigin(origins = "*")
    public ResponseEntity<PageResponse<Recipe>> getRecipes(
        @RequestParam("page") @Min(value = 1, message = "Min page value is 1") int page,
        @RequestParam("size") @Min(value = 1, message = "Min page size value is 1") int size,
        @RequestParam(value = "query", required = false) String query,
        @RequestParam(value = "orderBy", required = false) String orderBy,
        @RequestParam(value = "direction", required = false) Direction direction) {

        FilterRequest filterRequest = FilterRequest.builder().page(page - 1).size(size)
            .orderBy(orderBy).direction(direction).query(query).build();

        return ResponseEntity.ok(recipeService.getRecipes(filterRequest));
    }
}
